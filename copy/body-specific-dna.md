**BODY SPECIFIC DNA**™ was developed by Dr. Roxanne G. Carfora & is a Medically Managed

### _Health Transformation Program_

The program is based on the _Pathway Fit_™ saliva DNA test measuring over 75 Genetic Factors which determine YOUR body’s EXACT nutritional plan.

The DNA results are analyzed in conjunction with your full blood panel including vitamins, minerals & hormonal levels.

Dr. Carfora combines the DNA results & the blood results with the newest technologies for detection of early disease based on YOUR individual results!

Those technologies are:

* ZYTO BIO-COMMUNICATION SYSTEMS

* REEVUE METABOLIC TESTING

* SUDOSCANNING

After your full medical exam and results of the tests arrive, you are seen by a **HEALTH NUTRITIONAL COUNSELOR** trained in DNA testing and report interpretation. You will have all the information to stay healthy for the **REST of YOUR LIFE**!

**NO MORE GUESS WORK ON WHAT’S RIGHT FOR YOUR BODY TYPE!**

**YOU ARE BODY SPECIFIC!!!**
