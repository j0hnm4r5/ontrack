The **ReeVue** measures the oxygen that the body consumes. Using this measurement it calculates a patient’s _Resting Energy Expenditure_ (REE), commonly referred to as a _Resting Metabolic Rate_ (RMR). Physicians can screen for abnormally low metabolic rates, teach energy balance, and pinpoint the precise caloric intake required for weight loss.

Applications include obesity treatment, as well as treating obesity related diseases such as:
* diabetes
* dysmetabolic syndrome X
* hypothyroidism
* hyperthyroidism
* hypertension
* cardiovascular disease
* sleep apnea

Under strict laboratory protocol, the ReeVue can be used to measure _Basal Metabolic Rate_ (BMR).