var express = require('express');
var router = express.Router();

/* GET Shops. */
router.get('/', function(req, res) {
	res.render('basic', {
		title: 'Shops',
		content: 'Welcome to Shops'
	});
});

/* GET Vitamin Shoppe. */
router.get('/vitamin-shoppe', function(req, res) {
	// res.render('basic', {
	// 	title: 'Vitamin Shoppe',
	// 	content: 'Welcome to Vitamin Shoppe'
	// });

	// FORWARDING ALL TRAFFIC TO EXTERNAL SITE. CHANGE IF VENDORS ARE ADDED TO VITAMIN SHOPPE
	res.redirect('http://rcarfora.metagenics.com/');
});

/* GET Doterra. */
router.get('/doterra', function(req, res) {
	res.redirect('http://www.mydoterra.com/ontrackhealthandwellness/');
});

/* GET Juice Plus. */
router.get('/juice-plus', function(req, res) {
	res.redirect('http://www.juiceplus.com/content/JuicePlus/en/buy.html');
});

module.exports = router;
