var express = require('express');
var router = express.Router();

var marked = require('marked');
var fs = require('fs');

/* GET Programs. */
router.get('/', function(req, res) {
	res.render('basic', {
		title: 'Programs',
		content: 'Welcome to Programs'
	});
});

/* GET Body Specific DNA. */
router.get('/body-specific-dna', function(req, res) {
	res.render('basic', {
		title: 'Body Specific DNA',
		imageurl: '/images/body-specific-dna.png',
		content: marked(fs.readFileSync('copy/body-specific-dna.md', 'utf8'))
	});
});

/* GET Body Specific 360. */
router.get('/body-specific-360', function(req, res) {
	res.render('basic', {
		// title: 'Body Specific 360',
		imageurl: '/images/construction.png',
		content: marked(fs.readFileSync('copy/under-construction.md', 'utf8'))
	});
});

module.exports = router;
