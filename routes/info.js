var express = require('express');
var router = express.Router();

/* GET Info. */
router.get('/', function(req, res) {
	res.render('basic', {
		title: 'Info',
		content: 'Welcome to Info'
	});
});

/* GET Trademarks. */
router.get('/trademarks', function(req, res) {
	res.render('basic', {
		title: 'Trademarks',
		content: 'Welcome to Trademarks'
	});
});

/* GET Advertise. */
router.get('/advertise', function(req, res) {
	res.render('basic', {
		title: 'Advertise',
		content: 'Welcome to Advertise'
	});
});

/* GET About. */
router.get('/about', function(req, res) {
	res.render('basic', {
		title: 'About',
		content: 'Welcome to About'
	});
});

/* GET Contact. */
router.get('/contact', function(req, res) {
	res.render('basic', {
		title: 'Contact',
		content: 'Welcome to Contact'
	});
});

module.exports = router;
