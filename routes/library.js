var express = require('express');
var router = express.Router();

/* GET library. */
router.get('/', function(req, res) {
	res.render('basic', {
		title: 'Library',
		content: 'Welcome to Library'
	});
});

/* GET Let's Eat. */
router.get('/lets-eat', function(req, res) {
	res.render('basic', {
		title: 'Let\'s Eat',
		content: 'Welcome to Let\'s Eat'
	});
});

/* GET Get Moving. */
router.get('/get-moving', function(req, res) {
	res.render('basic', {
		title: 'Get Moving',
		content: 'Welcome to Get Moving'
	});
});

/* GET Monthly Pass. */
router.get('/monthly-pass', function(req, res) {
	res.render('basic', {
		title: 'Monthly Pass',
		content: 'Welcome to Monthly Pass'
	});
});

module.exports = router;
