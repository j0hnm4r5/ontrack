var express = require('express');
var router = express.Router();

/* GET community. */
router.get('/', function(req, res) {
	res.render('basic', {
		title: 'Community',
		content: 'Welcome to Community'
	});
});

/* GET Q & A. */
router.get('/q-and-a', function(req, res) {
	res.render('basic', {
		title: 'Q & A',
		content: 'Welcome to Q & A'
	});
});

/* GET Newsletters. */
router.get('/newsletters', function(req, res) {
	res.render('basic', {
		title: 'Newsletters',
		content: 'Welcome to Newsletters'
	});
});

/* GET Blog. */
router.get('/blog', function(req, res) {
	res.redirect('http://ontrackhw.svbtle.com/');
});

/* GET Success Stories. */
router.get('/success-stories', function(req, res) {
	res.render('basic', {
		title: 'Success Stories',
		content: 'Welcome to Success Stories'
	});
});

module.exports = router;
