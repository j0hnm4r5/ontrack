var express = require('express');
var router = express.Router();

var marked = require('marked');
var fs = require('fs');

/* GET tools. */
router.get('/', function(req, res) {
	res.render('basic', {
		title: 'Welcome',
		content: 'Welcome to Tools'
	});
});

/* GET ZYTO. */
router.get('/zyto', function(req, res) {
	res.redirect('http://www.zyto.com/');
});

/* GET Metabolic Testing. */
router.get('/metabolic-testing', function(req, res) {
	res.render('basic', {
		title: 'Reevue Metabolic Testing',
		imageurl: '/images/metabolic-testing.jpg',
		content: marked(fs.readFileSync('copy/metabolic-testing.md', 'utf8'))
	});
});

/* GET Sudo Scan. */
router.get('/sudo-scan', function(req, res) {
	res.render('basic', {
		title: 'Sudoscan',
		imageurl: '/images/sudoscan.png',
		content: marked(fs.readFileSync('copy/sudoscan.md', 'utf8'))
	});
});

/* GET Pathway Fit. */
router.get('/pathway-fit', function(req, res) {
	res.redirect('http://connect.pathway.com/reports/pathway-fit/');
});

module.exports = router;
